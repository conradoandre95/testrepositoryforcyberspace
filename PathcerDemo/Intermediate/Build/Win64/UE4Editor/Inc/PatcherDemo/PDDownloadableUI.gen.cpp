// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PatcherDemo/PDDownloadableUI.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePDDownloadableUI() {}
// Cross Module References
	PATCHERDEMO_API UClass* Z_Construct_UClass_UPDDownloadableUI_NoRegister();
	PATCHERDEMO_API UClass* Z_Construct_UClass_UPDDownloadableUI();
	UMG_API UClass* Z_Construct_UClass_UUserWidget();
	UPackage* Z_Construct_UPackage__Script_PatcherDemo();
// End Cross Module References
	void UPDDownloadableUI::StaticRegisterNativesUPDDownloadableUI()
	{
	}
	UClass* Z_Construct_UClass_UPDDownloadableUI_NoRegister()
	{
		return UPDDownloadableUI::StaticClass();
	}
	struct Z_Construct_UClass_UPDDownloadableUI_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPDDownloadableUI_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UUserWidget,
		(UObject* (*)())Z_Construct_UPackage__Script_PatcherDemo,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPDDownloadableUI_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "PDDownloadableUI.h" },
		{ "ModuleRelativePath", "PDDownloadableUI.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPDDownloadableUI_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPDDownloadableUI>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPDDownloadableUI_Statics::ClassParams = {
		&UPDDownloadableUI::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPDDownloadableUI_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPDDownloadableUI_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPDDownloadableUI()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPDDownloadableUI_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPDDownloadableUI, 1431577155);
	template<> PATCHERDEMO_API UClass* StaticClass<UPDDownloadableUI>()
	{
		return UPDDownloadableUI::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPDDownloadableUI(Z_Construct_UClass_UPDDownloadableUI, &UPDDownloadableUI::StaticClass, TEXT("/Script/PatcherDemo"), TEXT("UPDDownloadableUI"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPDDownloadableUI);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
