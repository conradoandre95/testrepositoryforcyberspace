// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PATCHERDEMO_PatcherDemoGameModeBase_generated_h
#error "PatcherDemoGameModeBase.generated.h already included, missing '#pragma once' in PatcherDemoGameModeBase.h"
#endif
#define PATCHERDEMO_PatcherDemoGameModeBase_generated_h

#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_SPARSE_DATA
#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTryServerTravel);


#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTryServerTravel);


#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPatcherDemoGameModeBase(); \
	friend struct Z_Construct_UClass_APatcherDemoGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APatcherDemoGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PatcherDemo"), NO_API) \
	DECLARE_SERIALIZER(APatcherDemoGameModeBase)


#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPatcherDemoGameModeBase(); \
	friend struct Z_Construct_UClass_APatcherDemoGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APatcherDemoGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PatcherDemo"), NO_API) \
	DECLARE_SERIALIZER(APatcherDemoGameModeBase)


#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APatcherDemoGameModeBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APatcherDemoGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APatcherDemoGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APatcherDemoGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APatcherDemoGameModeBase(APatcherDemoGameModeBase&&); \
	NO_API APatcherDemoGameModeBase(const APatcherDemoGameModeBase&); \
public:


#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APatcherDemoGameModeBase(APatcherDemoGameModeBase&&); \
	NO_API APatcherDemoGameModeBase(const APatcherDemoGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APatcherDemoGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APatcherDemoGameModeBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APatcherDemoGameModeBase)


#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_12_PROLOG
#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_SPARSE_DATA \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_RPC_WRAPPERS \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_INCLASS \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_SPARSE_DATA \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PATCHERDEMO_API UClass* StaticClass<class APatcherDemoGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PathcerDemo_Source_PatcherDemo_PatcherDemoGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
